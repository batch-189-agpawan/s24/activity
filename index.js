// alert('hello')
let number = 2
let getCube = (number)**3

console.log(`The cube of ${number} is ${getCube}`)

let address = {
	streetNumber: 258,
	streetName:"Washington Ave NW,",
	country: "California",
	zipcode: 90011
}

function fullAddress({streetNumber,streetName,country,zipcode}){
	console.log(`I live at ${streetNumber} ${streetName} ${country} ${zipcode}`)
}

fullAddress(address)

let animal = {
	name: "Lolong",
	animalClass:"saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in."
}

function animalDetails({name,animalClass,weight,measurement}){
	console.log(`${name} is a ${animalClass}. He weighed ${weight} kgs with a measurement of ${measurement}`)
}

animalDetails(animal)

let numbers = [1,2,3,4,5]

numbers.forEach(function(number){
	console.log(number)
})

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}

let myDog = new Dog();

myDog.name = "Bruno"
myDog.age = 2
myDog.breed = "Alaskan Malamute"

console.log(myDog)